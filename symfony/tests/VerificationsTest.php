<?php

namespace App\Tests;

use App\Entity\Verification;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class VerificationsTest extends AbstractTest
{
    /**
     * as a registered user, i can request a verification
     * self::USER3_CREDENTIALS, this user has no verification
     */
    public function testCanRequestVerification()
    {
        $filename = 'fixtures/files/logo.jpeg';
        $file = new UploadedFile($filename, 'logo.jpeg');

        $this->createClientWithCredentials(self::USER3_CREDENTIALS)
            ->request('POST', '/api/verifications', [
                'headers' => ['Content-Type' => 'multipart/form-data'],
                'extra' => [
                    'parameters' => ['message' => 'My verification message'],
                    'files' => ['imageFile' => $file],
                ]
            ]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Verification::class);
        $this->assertJsonContains([
            'message' => 'My verification message',
            'imageName' => 'logo.jpeg',
        ]);
        $fileSystem = new Filesystem();
        $fileSystem->rename('public/verification_images/logo.jpeg', $filename);
    }

    /**
     * as a user that already had a verification, i cant' request a new verification
     */
    public function testCantRequestVerification()
    {
        $this->createClientWithCredentials(self::USER1_CREDENTIALS)
            ->request('POST', '/api/verifications', [
                'headers' => ['Content-Type' => 'multipart/form-data'],
                'extra' => [
                    'parameters' => [],
                    'files' => [],
                ]
            ]);
        $this->assertResponseStatusCodeSame(400);
    }

    /**
     * as a user that already had an approved verification, i cant' edit my verification
     */
    public function testCantEditApprovedVerification()
    {
        $body = ['message' => 'My verification message edited'];
        $this->createClientWithCredentials(self::BLOGGER1_CREDENTIALS)
            ->request('PUT', '/api/verifications/1', [
                'headers' => ['Content-Type' => 'multipart/form-data'],
                'extra' => [
                    'parameters' => $body,
                    'files' => [],
                ]
            ]);
        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * as a registered user, i can edit my verification request
     */
    public function testCanEditMyOwnVerification()
    {
        $body = ['message' => 'My verification message edited'];
        $this->createClientWithCredentials(self::USER1_CREDENTIALS)
            ->request('PUT', '/api/verifications/1', [
                'headers' => ['Content-Type' => 'multipart/form-data'],
                'extra' => [
                    'parameters' => $body,
                    'files' => [],
                ]
            ]);

        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Verification::class);
        $this->assertJsonContains($body);
    }

    /**
     * as a registered user, i can't edit other verification request
     */
    public function testCantEditOtherVerification()
    {
        $body = ['message' => 'My verification message edited'];
        $this->createClientWithCredentials(self::USER2_CREDENTIALS)
            ->request('PUT', '/api/verifications/1', [
                'headers' => ['Content-Type' => 'multipart/form-data'],
                'extra' => [
                    'parameters' => $body,
                    'files' => [],
                ]
            ]);
        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * as an admin, i can see the verification request list
     */
    public function testAdminCanSeeVerificationList()
    {
        $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('GET', '/api/verifications');
        $this->assertResponseIsSuccessful();
    }

    /**
     * as an admin, i can filter the verification request list by user and status
     */
    public function testAdminCanFilterVerificationList()
    {
        $response = $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('GET', '/api/verifications?owner=1');
        $this->assertEquals(1, count(json_decode($response->getContent(), true)['hydra:member']));

        $response = $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('GET', '/api/verifications?status=approved');
        $this->assertEquals(1, count(json_decode($response->getContent(), true)['hydra:member']));
    }

    /**
     * as an admin, i can order the verification request list by created_at 
     */
    public function testAdminCanOrderVerificationList()
    {
        $response = $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('GET', '/api/verifications?order[createdAt]=desc');
        $data = json_decode($response->getContent(), true)['hydra:member'];

        $this->assertGreaterThanOrEqual($data[count($data) - 1]['createdAt'], $data[0]['createdAt']);
    }

    /**
     * as an admin, i can approve a verification
     */
    public function testAdminCanApproveVerification()
    {
        $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('PATCH', '/api/verifications/1/state-change', [
            'json' => ['status' => Verification::APPROVED_STATUS]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['status' => Verification::APPROVED_STATUS]);
        $this->assertEmailCount(1);
    }

    /**
     * as an admin, i can decline a verification
     */
    public function testAdminCanDeclineVerification()
    {
        $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('PATCH', '/api/verifications/1/state-change', [
            'json' => ['status' => Verification::DECLINED_STATUS]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['status' => Verification::DECLINED_STATUS]);
        $this->assertEmailCount(1);
    }

    /**
     * as an admin, i can't decline a verification with wrong or empty status
     */
    public function testAdminCantChangeVerificationStateWithEmptyOrWrongStatus()
    {
        $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('PATCH', '/api/verifications/1/state-change', [
            'json' => []
        ]);
        $this->assertResponseStatusCodeSame(500);
        $this->createClientWithCredentials(self::ADMIN_CREDENTIALS)->request('PATCH', '/api/verifications/1/state-change', [
            'json' => ['status' => 'falsy_status']
        ]);
        $this->assertResponseIsSuccessful(500);
    }
}
