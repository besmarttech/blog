<?php

namespace App\Tests;

class SecurityTest extends AbstractTest
{
    /**
     * as a guest, i can register
     */
    public function testCanRegister()
    {
        static::createClient()->request('POST', '/api/users', ['json' => [
            'email' => 'test@example.com',
            'firstname' => 'My first Name',
            'lastname' => 'My last Name',
            'password' => 'MyPassword'
        ]]);

        $this->assertResponseIsSuccessful();
    }

    /**
     * as a guest, i can't register with an existing email
     */
    public function testCantRegisterWithAnExistingEmail()
    {
        static::createClient()->request('POST', '/api/users', ['json' => [
            'firstname' => 'My first Name',
            'lastname' => 'My last Name'
        ]+self::USER1_CREDENTIALS]);
        $this->assertResponseStatusCodeSame(422);
    }

    /**
     * as an existing user, i can login
     */
    public function testCanLogin()
    {
        static::createClient()->request('POST', '/api/login_check', ['json' => self::USER1_CREDENTIALS]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * as a non existing user, i can't login
     */
    public function testCantLogin()
    {
        static::createClient()->request('POST', '/api/login_check', ['json' => self::WRONG_CREDENTIALS]);
        $this->assertResponseStatusCodeSame(401);
    }
}
