<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

abstract class AbstractTest extends ApiTestCase
{
    const WRONG_CREDENTIALS = [
        'email' => 'dummy@localhost.local',
        'password' => 'dummy-password'
    ];

    const USER1_CREDENTIALS = [
        'email' => 'user1@localhost.local',
        'password' => 'test'
    ];

    const USER2_CREDENTIALS = [
        'email' => 'user2@localhost.local',
        'password' => 'test'
    ];

    const USER3_CREDENTIALS = [
        'email' => 'user3@localhost.local',
        'password' => 'test'
    ];

    const BLOGGER1_CREDENTIALS = [
        'email' => 'blogger1@localhost.local',
        'password' => 'test'
    ];

    const BLOGGER2_CREDENTIALS = [
        'email' => 'blogger2@localhost.local',
        'password' => 'test'
    ];

    const ADMIN_CREDENTIALS = [
        'email' => 'admin@localhost.local',
        'password' => 'test'
    ];

    private $token;
    private $clientWithCredentials;

    use RefreshDatabaseTrait;

    public function setUp(): void
    {
        self::bootKernel();
    }

    protected function createClientWithCredentials($credentials = null): Client
    {
        return static::createClient([], ['headers' => ['authorization' => 'Bearer ' . $this->getToken($credentials)]]);
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken($credentials = null): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response =  static::createClient()->request('POST', '/api/login_check', ['json' => $credentials ? $credentials : self::USER1_CREDENTIALS]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }
}
