<?php

namespace App\Tests;

class PostsTest extends AbstractTest
{
    /**
     * as an an authenticated user, i can see all blog posts 
     */
    public function testCanSeeAllPosts()
    {
        $this->createClientWithCredentials()->request('GET', '/api/posts');

        $this->assertResponseIsSuccessful();
    }

    /**
     * as a guest, i can't see any posts
     */
    public function testCantSeeAnyPost()
    {
        static::createClient()->request('GET', '/api/posts');
        $this->assertResponseStatusCodeSame(401);
    }

    /**
     * as a blogger, i can add a new blog post 
     */
    public function testCanAddNewPost()
    {
        $this->createClientWithCredentials(self::BLOGGER1_CREDENTIALS)
            ->request('POST', '/api/posts', ['json' => [
                'title' => 'My post title',
                'content' => 'My post content'
            ]]);

        $this->assertResponseIsSuccessful();
    }

    /**
     * as a user, i can't add a new blog post yet
     */
    public function testCantAddNewPost()
    {
        $this->createClientWithCredentials(self::USER1_CREDENTIALS)
            ->request('POST', '/api/posts', ['json' => [
                'title' => 'My post title',
                'content' => 'My post content'
            ]]);

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * as a verified user, i can edit my own blog post 
     */
    public function testCanEditMyOwnPost()
    {
        $this->createClientWithCredentials(self::BLOGGER1_CREDENTIALS)
            ->request('PUT', '/api/posts/1', ['json' => [
                'title' => 'My post title',
                'content' => 'My post content'
            ]]);

        $this->assertResponseIsSuccessful();
    }

    public function testCantEditOthersPost()
    {
        $this->createClientWithCredentials(self::BLOGGER2_CREDENTIALS)
            ->request('PUT', '/api/posts/1', ['json' => [
                'title' => 'My post title',
                'content' => 'My post content'
            ]]);

        $this->assertResponseStatusCodeSame(403);
    }
}
