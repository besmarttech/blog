<?php

use App\Entity\User;

interface OwnerInterface
{
    public function setOwnerData(User $user): self;
}
