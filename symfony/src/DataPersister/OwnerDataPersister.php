<?php

namespace App\DataPersister;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\ResumableDataPersisterInterface;
use App\Entity\Post;
use OwnerInterface;
use ReflectionMethod;
use Symfony\Component\Security\Core\Security;

/**
 * This custom persister will look for the setOwnerData method in the current entity
 * and populate it with the current User 
 */
class OwnerDataPersister implements ContextAwareDataPersisterInterface, ResumableDataPersisterInterface
{
    private $em;
    private $security;

    public function __construct(
        EntityManagerInterface $em,
        Security $security
    ) {
        $this->em = $em;
        $this->security = $security;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof OwnerInterface;
    }

    public function persist($data, array $context = []): OwnerInterface
    {
        $reflectionMethod = new ReflectionMethod($data, "setOwnerData");
        $reflectionMethod->invoke($data, $this->getUser());
        $this->em->persist($data);

        return $data;
    }

    public function remove($data, array $context = [])
    {
    }

    private function getUser(): ?User
    {
        return $this->security->getUser();
    }

    public function resumable(array $context = []): bool
    {
        return true;
    }
}
