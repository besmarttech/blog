<?php

namespace App\Verification;

use App\Entity\Verification;
use ErrorException;
use LogicException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class VerificationStateStrategyManager
{
    private $strategyByStatus = [];

    public function __construct(
        VerificationApproveStrategy $approveStrategy,
        VerificationDeclineStrategy $declineStrategy
    ) {
        $this->strategyByStatus = [
            Verification::APPROVED_STATUS => $approveStrategy,
            Verification::DECLINED_STATUS => $declineStrategy
        ];
    }

    public function handle(Verification $verification, $status)
    {
        if (!array_key_exists($status, $this->strategyByStatus)){
            return new HttpException(500, "State not found.");
        }
        
        $startegy = $this->strategyByStatus[$status];

        if (! ($startegy instanceof VerificationStateInterface) ){
            return new HttpException(500, "State not allowed.");
        }
        
        return $startegy->handle($verification);
    }
}