<?php

namespace App\Verification;

use App\Entity\Verification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class VerificationApproveStrategy implements VerificationStateInterface
{
    private EntityManagerInterface $em;
    private MailerInterface $mailer;

    public function __construct(EntityManagerInterface $em, MailerInterface $mailer)
    {
        $this->em = $em;
        $this->mailer = $mailer;
    }

    public function handle(Verification $verification): Verification
    {
        $verification->setStatus(Verification::APPROVED_STATUS);
        $this->em->persist($verification);
        $this->em->flush();
        $this->sendEmail($verification);

        return $verification;
    }

    private function sendEmail(Verification $verification)
    {
        $owner = $verification->getOwner();
        $email = (new Email())
            ->from('hello@example.com')
            ->to($owner->getEmail())
            ->subject('Verification Approved')
            ->text(sprintf('Congratulation %s, your verification has been approved', $owner));

        $this->mailer->send($email);
    }
}
