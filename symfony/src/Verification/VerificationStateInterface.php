<?php
namespace App\Verification;

use App\Entity\Verification;

interface VerificationStateInterface {
    public function handle(Verification $verification);
}