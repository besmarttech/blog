<?php

namespace App\Exception;

use LogicException;

final class VerificationExistException extends LogicException
{
}