<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\VerificationStateController;
use App\Repository\VerificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use OwnerInterface;

/**
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: VerificationRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ApiResource(
    normalizationContext: ['groups' => ['verification:read']],
    denormalizationContext: ['groups' => ['verification:write']],
    collectionOperations: [
        "get",
        'post' => [
            'input_formats' => [
                'multipart' => ['multipart/form-data'],
            ]
        ],
    ],
    itemOperations: [
        "get" => ["security" => "is_granted('VERIFICATION_VIEW', object)"],
        "put" => [
            "security" => "is_granted('VERIFICATION_EDIT', object)",
            'input_formats' => [
                'multipart' => ['multipart/form-data'],
            ]
        ],
        "state-change" => [
            "security" => "is_granted('VERIFICATION_EDIT', object)",
            "attributes" => ["input_formats" => ["json" => ["application/ld+json", "application/json"]]],
            "method" => "PATCH",
            "path" => "/verifications/{id}/state-change",
            "controller" => VerificationStateController::class
        ],
    ],
)]
#[ApiFilter(SearchFilter::class, properties: ['owner' => 'exact', 'status' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['createdAt'], arguments: ['orderParameterName' => 'order'])]
class Verification implements OwnerInterface
{
    const PENDING_STATUS = 'pending';
    const APPROVED_STATUS = 'approved';
    const DECLINED_STATUS = 'declined';

    const STATUS = [
        self::PENDING_STATUS => 'Pending',
        self::APPROVED_STATUS => 'Approved',
        self::DECLINED_STATUS => 'Declined'
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(["verification:read", "verification:write"])]
    private $message;

    #[ORM\OneToOne(inversedBy: 'verification', targetEntity: User::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["verification:read"])]
    private $owner;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(["verification:read"])]
    private $createdAt;

    #[ORM\Column(type: 'string', length: 30)]
    #[Groups(["verification:read"])]
    private $status = self::PENDING_STATUS;

    /**
     * @Vich\UploadableField(mapping="verification_images", fileNameProperty="imageName", size="imageSize")
     */
    #[Groups(['verification:write'])]
    private ?File $imageFile = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(["verification:read"])]
    private ?string $imageName = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $imageSize = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    #[ORM\PrePersist]
    public function updateCreatedAt()
    {
        $now = new \DateTimeImmutable();
        $this->createdAt = $now;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function isPending()
    {
        return $this->status == self::PENDING_STATUS;
    }

    public function setOwnerData(User $user): self
    {
        $this->owner = $user;
        
        return $this;   
    }
}
