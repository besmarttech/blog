<?php

namespace App\Controller;


use App\Entity\Verification;
use App\Verification\VerificationStateStrategyManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\HttpException;

#[AsController]
class VerificationStateController extends AbstractController
{
    private VerificationStateStrategyManager $strategyManager;
    public function __construct(VerificationStateStrategyManager $strategyManager)
    {
        $this->strategyManager = $strategyManager;
    }

    public function __invoke($data, Request $request)
    {
        if (!$request->getContent() 
        || !is_array($patchData = json_decode($request->getContent(), true))
        || !isset($patchData['status'])){
            throw new HttpException(500, "missing status parameter.");
        }

        return $this->strategyManager->handle($data, $patchData['status']);
    }
}