<?php
namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Entity\Verification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

final class VerificationSubscriber implements EventSubscriberInterface
{
    private $em;
    private $security;

    public function __construct(
        EntityManagerInterface $em,
        Security $security    
    ) {
        $this->em = $em;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['preVerificationWrite', EventPriorities::POST_VALIDATE],
        ];
    }

    public function preVerificationWrite(ViewEvent $event): void
    {
        $verification = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!($verification instanceof Verification) 
        || !in_array($method, [Request::METHOD_PUT, Request::METHOD_POST])) {
            return;
        }

        if (!$verification->getId() && $this->getUser()->getVerification()){
            throw new HttpException(400, "Verification already exist");
        }

        if ($verification->getId() 
            && !$this->em->getRepository(Verification::class)->find($verification->getId())->isPending()){
            throw new HttpException(500, sprintf("Can't edit a non pending verification %s", $verification->getId()));
        }
    }

    private function getUser(): ?User
    {
        return $this->security->getUser();
    }
}