## Requirements
You need a few things to get started:

* Install Docker

* Install Docker-compose

## Installation
blog_app_php-fpm is the php container name
### Clone the project
`git clone git@bitbucket.org:besmarttech/blog.git`
### Edit configuration
There are three config file to fill out:

* cp blog-docker/.env.dist blog-docker/.env

* cp symfony/.env blog-docker/.env.local

* cp symfony/.env blog-docker/.env.test

## Run docker-compose & install dependencies
`cd blog-docker && docker-compose up -d`

`docker exec -it blog_app_php-fpm composer i`

### Generate JWT key
`docker exec -it blog_app_php-fpm php bin/console lexik:jwt:generate-keypair`

## Testing
### Prerequisites
Tests use fixtures

* docker exec -it blog_app_php-fpm  php bin/console d:d:d --force --env=test

* docker exec -it blog_app_php-fpm  php bin/console d:d:c --env=test

* docker exec -it blog_app_php-fpm  php bin/console d:m:m --env=test

* docker exec -it blog_app_php-fpm  php bin/console hautelook:fixtures:load --env=test

### Run test
`docker exec -it blog_app_php-fpm php bin/phpunit`
